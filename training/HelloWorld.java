package training;

public class HelloWorld {
    public static void main(String[] args) {
        // Printing Strings
        System.out.println("Hello world!");
        System.out.println("This is some more text. ");

        System.out.print("This is not on a new line. ");

        // Maths
        System.out.println(37462);
        System.out.println(7+2); // Addition
        System.out.println(5*5); // Multiplication

        /*
        This is a
        multi line
        comment 
        */
    }
}
