package training;
public class Variables {
    public static void main(String[] args) {
        
        // Different data types as variables
        String string = "I am a string";
        int integer = 4;
        double decimal = 16.65;
        char character = 'g';
        boolean isTrue = true;
        final int unchangable = 67;

        // Printing the variables
        System.out.println(string);
        System.out.println(integer);
        System.out.println(decimal);
        System.out.println(character);
        System.out.println(isTrue);
        System.out.println(unchangable);
    }
}
